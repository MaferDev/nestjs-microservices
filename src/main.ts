import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { CustomExceptionFilter } from './common/filters/custom-exception.filter';
import { AllExceptionFilter } from './common/filters/http-exception.filter';
import { TimeOutInterceptor } from './common/interceptors/timeout.interceptor';
import { ExceptionsError } from './helpers/ExceptionsError';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalFilters(
    new AllExceptionFilter(),
    new CustomExceptionFilter(),
    // new AlladapterExceptionsFilter(adapterHost),
  );
  app.useGlobalInterceptors(new TimeOutInterceptor());

  console.log(ExceptionsError.NOT_AUTH.code);

  await app.listen(3000);
}
bootstrap();
