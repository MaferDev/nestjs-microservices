import {
  Body,
  Controller,
  Get,
  Post,
  Param,
  Put,
  Delete,
  ForbiddenException,
  HttpException,
} from '@nestjs/common';
import { TaskService } from './task.service';
import { TaskDTO } from '../task/dto/task.dto';
import { CustomError } from 'src/common/filters/custom-exception.filter';

@Controller('api/v1/task')
export class TaskController {
  constructor(private taskService: TaskService) {}
  @Post()
  create(@Body() taskDTO: TaskDTO) {
    return this.taskService.create(taskDTO);
    // return new Promise((resolve, reject) => {
    //   setTimeout(() => reject('Error de Petición'), 2000);
    // });
  }

  @Post('/error-exception')
  errorException(@Body() taskDTO: TaskDTO) {
    throw new ForbiddenException();
  }

  @Post('/error-exception-our')
  // @UseFilters(new AllExceptionFilter(), new CustomExceptionFilter())
  ourErrorException(@Body() taskDTO: TaskDTO) {
    try {
      throw new HttpException('Error de prueba', 402);
    } catch (err) {
      throw new CustomError('Error_01', err);
    }
  }

  @Get()
  findAll() {
    return this.taskService.findAll();
  }

  @Get('/:id')
  findOne(@Param('id') id: string) {
    return this.taskService.findOne(id);
  }

  @Put('/:id')
  update(@Param('id') id: string, @Body() body: TaskDTO) {
    return this.taskService.update(id, body);
  }

  @Delete('/:id')
  delete(@Param('id') id: string) {
    return this.taskService.delete(id);
  }
}
