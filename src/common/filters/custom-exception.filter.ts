import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
} from '@nestjs/common';

export class CustomError {
  private codeError: string;
  private response: string;
  private status: number;
  private stack?: string;

  constructor(codeError: string, error: any) {
    this.codeError = codeError;
    this.response = error.getResponse();
    this.status = error.getStatus();
    this.stack = error.stack;

    // eslint-disable-next-line prefer-rest-params
    Error.apply(this, arguments);
  }

  getCodeError(): string {
    return this.codeError;
  }

  getStatus(): number {
    return this.status || HttpStatus.INTERNAL_SERVER_ERROR;
  }

  getResponse(): string {
    return this.response || 'ERROR';
  }

  getStack(): string {
    return this.stack;
  }
}

@Catch(CustomError)
export class CustomExceptionFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    console.log(exception);

    const context = host.switchToHttp();
    const response = context.getResponse();
    const request = context.getRequest();

    // // Catch Status
    const status =
      exception instanceof CustomError
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;
    const msg =
      exception instanceof CustomError
        ? exception.getResponse()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    response.status(status).json({
      statusCode: status,
      timestamp: new Date().toISOString(),
      path: request.url,
      codeError: exception.getCodeError(),
      error: msg,
    });
  }
}
