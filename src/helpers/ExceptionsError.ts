class errorType {
  code: string;
  message: string;
}

export abstract class ExceptionsError {
  static NOT_AUTH: errorType = {
    code: 'BBFF_AUTH_001',
    message: 'Usuario no autentificado',
  };
  // static paramStoreAPIVersion = '2014-11-06';
  // static duration = 120;
  // static region = 'us-west-1';
  // static path = '/myTataLambda/';
  // static baseUrl = 'https://swapi.py4e.com/api/';
}
