export interface ITask {
  readonly id?: string;
  readonly description: string;
  readonly isDone: boolean;
}
